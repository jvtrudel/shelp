# SHELP  -- Shell scripts helper cli

CLI tools to manage libraries of shell/bash code.


# USAGE

## Source your code

Commands for retrieving and accessing your code.
Code is sent to stdout so it can be sourced, redirected to file or piped to other commands.

    # Source code in shelp's core library
    . <(shelp lib core fatal,msg,err,warning)

    # Source code a shelp compliant library
    #   located in another directory
    . <(shelp -C /path/to/my/io/lib lib io read_,write_)

## Shelp templates

Generate templates of shelp conformant files and directories

   # export path string
   shelp export path [<exec_path>]

   # generate from template
   shelp generate <template file> <template function> [options]


## Shelp validators

Validate than files or directories are shelp conformants.

## Shelp reflexivity commands

Helpful commands.

    # Show help/cheatsheet
    shelp

    # show runtime shelp variables
    shelp config

    # show shelp version
    shelp version

# INSTALLATION

Implementation is limited to in- source execution. Just put the bin directory in your source.

A convenience command generate and export string:

    shelp export path [<exec_dir>]


# Source compatible avec shelp

Shelp fournit un standard de compatibilité entre diverses sources de scripts bash afin d'en facilité le réemploie et la gestion.

## Shelp est extensible

Shelp ne définit pas tout ce qui peut exister, mais seulement les fonctionalités minimales.

Le namespace est le nom de la source ou une abbréviation. La variable `$NAMESPACE_PATH` fait référence à la racine du projet.

## Les sources

Les sources sont des codes sources, généralement des repos git, compatible avec shelp.

## Les fonctions

Dans le répertoire `$NAMESPACE_PATH/functions` se trouve des collection de fonctions. Ces fonctions peuvent être organisées dans plusieurs répertoires, sous-répertoires et fichiers.

    # rend disponible les fonctions dans <dir/file> du source <namespace>
    . <(shelp func <namespace> <dir/file>)

C'est fonctions peuvent être utilisées sans références externes.

## les librairies


Les librairies sont des regroupements de fonctions cohérentes et ayant des interdépendances. Ils nécessitent le chargement des variables de NAMESPACE.

## Les collections

Les collections sont des regroupements de fonctions cohérentes et ayant des dépendances sur des projets externes. Ils utilisent généralement shelp pour charger leurs dépendances.

## Les commandes


Les commandes offre une interface en ligne de commande pour les fonctions ou les collections ou tout autre commande implicant une logique front-end.


## Fonctionalité minimale

**usage**: retourne les instructions pour utilisé le cli.

**version**: retourne le numéro de la version courante.

**config**: retourne les variables d'environnement du script.



(À terme, c'est fonctionalités devraient pouvoir être fournies par shelp)



# Commodité sur les sources


## Importation à partir d'une source git

    shelp import git@gitlab.com:jvtrudel/shelp.git

## Exporte le path dans le fichier bashrc

    shelp source bashrc <namespace>
